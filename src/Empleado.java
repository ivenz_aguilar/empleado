public class Empleado {

    //Atributos
    private String cedula;
    private String nombre;
    private String puesto;

    //Constructores

    public Empleado() {
        this.cedula="";
        this.nombre="";
        this.puesto="";
    }

    public Empleado(String cedula, String nombre, String puesto) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.puesto = puesto;
    }

    //Get and Set (metodos de acceso)

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }


    //ToString()

    @Override
    public String toString() {
        return "cedula='" + cedula  +", nombre='" + nombre  +", puesto='" + puesto;
    }
}
