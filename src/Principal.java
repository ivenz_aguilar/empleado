import java.io.*;

public class Principal {

    //atributos
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static Empleado[] listaEmpleados = new Empleado[10];

    public static void main(String[] args) throws IOException{

        menu();
    }

    public static void menu() throws IOException{
        int opcion = 0;
        do{
            System.out.println("Bienvenido al sistema");
            System.out.println("1. Registrar empleado");
            System.out.println("2. Listar empleados");
            System.out.println("3. Salir");
            System.out.print("Digite una opcion: ");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        }while(opcion != 3);
    }

    public static void procesarOpcion(int opcion) throws IOException{
        switch (opcion)
        {
            case 1: registrarEmpleado();
            break;
            case 2: listarEmpleados();
            break;
            case 3:
                System.out.println("Gracias vuelva pronto!");
                break;
            default:
                System.out.println("Opcion invalida, vuelva a intentarlo");
        }
    }
    //Capturar datos
    public static void registrarEmpleado() throws IOException{
        System.out.print("Ingrese la cedula: ");
        String cedula = in.readLine();
        System.out.print("Ingrese la nombre: ");
        String nombre = in.readLine();
        System.out.print("Ingrese la puesto: ");
        String puesto = in.readLine();

        //Creo objeto
        Empleado empleado = new Empleado(cedula, nombre, puesto);

        //Lo inserto en un espacio vacio del arreglo
        for(int x = 0; x < listaEmpleados.length;x++){
            if(listaEmpleados[x] == null){
                listaEmpleados[x] = empleado;
                break; //para cortar el proceso
            }
        }
    }

    public static void listarEmpleados(){
        for( int i= 0; i < listaEmpleados.length; i++){
            if(listaEmpleados[i] != null){
                System.out.println(listaEmpleados[i].toString());
            }
        }
    }
}
